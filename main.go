package main

import (
	"bufio"
	"bytes"
	"encoding/csv"
	"fmt"
	"html/template"
	"io"
	"log"
	"math"
	"math/rand"
	"os"
	"path/filepath"
	"strings"
	"time"

	wkhtml "github.com/SebastiaanKlippert/go-wkhtmltopdf"
	"github.com/gookit/color"
)

type ReportOutput struct {
	VersioneProgramma string
	Identificativo    string
	RifTempInizio     string
	RifTempFine       string
	TotPossEstratti   int
	ListaPossEstratti string
	OraEdata          string
	PercEstratti      int
	TotEstratti       int
	ElencoEstratti    string
}

func printarray(a []string) {
	/*for _, v1 := range a {
		//fmt.Printf("%s ", v1)
		color.Style{color.FgLightYellow, color.OpBold}.Printf("%s, ", v1)
	}*/
	fmt.Printf("\n")
	color.Style{color.FgLightYellow, color.OpBold}.Printf("%s", strings.Join(a, ", "))
	fmt.Printf("\n")
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

func getpercentage() int {
	stdin := bufio.NewReader(os.Stdin)
	var input int
	for {
		fmt.Fscan(stdin, &input)

		if input < 0 || input > 100 {
			fmt.Print("Dato non valido. Inserire un numero da 1 a 100: ")
			stdin.ReadString('\r') //Necessario per i sistemi Windows
			stdin.ReadString('\n')
		} else {
			break
		}
	}
	return input
}

func getstring() string {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	text := scanner.Text()
	return text
}

func parsedate(s string) string {
	d, err := time.Parse("2006-01-02", s)
	if err != nil {
		color.Style{color.FgRed, color.OpBold}.Println("Errore: la data inserita non è valida. Usare il formato aaaa-mm-gg.\n")
		t := ""
		return t
		//return err.Error()
	}
	t := d.Format("2006-01-02")
	return t
}

func percent(percent int, all int) float64 {
	return ((float64(all) * float64(percent)) / float64(100))
}

func main() {

	var verProgramma string
	verProgramma = "Sorteggio 0.2.1"

	var percentEstratti int
	var nEstratti int
	var tag string
	var dataInizio string
	var dataFine string
	var listaScelte = []string{}
	var listaEstratti = []string{}

	csvFile, err := os.Open("aziende.csv")
	if err != nil {
		panic(err)
	}
	defer csvFile.Close()

	reader := csv.NewReader(bufio.NewReader(csvFile))

	for {
		line, error := reader.Read()
		if error == io.EOF {
			break
		} else if error != nil {
			log.Fatal(error)
		}
		listaScelte = append(listaScelte, line[0])

	}

	dataOra := time.Now()
	listaSceltelen := len(listaScelte)

	rand.Seed(time.Now().UnixNano())
	randomize := rand.Perm(len(listaScelte))

	color.Style{color.FgWhite, color.OpBold}.Println("----------------------------------")
	color.Style{color.FgLightMagenta, color.OpBold}.Printf("%s - Comune di Sestu ", verProgramma)
	fmt.Print("\n")
	color.Style{color.FgWhite, color.OpBold}.Println("----------------------------------")
	fmt.Print("\n")
	color.Style{color.FgCyan, color.OpBold}.Print("Inserire un codice identificativo del sorteggio (preme Invio per lasciare vuoto): ")
	tag = getstring()
	color.Style{color.FgCyan, color.OpBold}.Print("Periodo di riferimento")

	for {
		color.Style{color.FgCyan, color.OpBold}.Print(" da (inserire una data): ")
		dataInizio = parsedate(getstring())
		if dataInizio != "" {
			break
		}
	}

	for {
		color.Style{color.FgCyan, color.OpBold}.Print(" a (inserire una data): ")
		dataFine = parsedate(getstring())
		if dataFine != "" {
			break
		}
	}

	color.Style{color.FgCyan, color.OpBold}.Print("Inserire la percentuale di estratti: ")
	percentEstratti = getpercentage()

	//nEstratti = int(float64(percentEstratti) * (float64(listaSceltelen) / float64(100)))

	//Calcola la percentuale di estratti e prende solo la parte intera del risultato
	nEstratti = int(math.Round(percent(percentEstratti, listaSceltelen)))

	//Fa in modo che se la percentuale di estratti risulta zero perchè troppo bassa rispetto alla
	//lista scelte, venga estratto almento un elemento.
	if listaSceltelen != 0 && percentEstratti != 0 && nEstratti == 0 {
		nEstratti = 1
	}

	fmt.Print("\n")
	color.Style{color.FgCyan, color.OpBold}.Print("Identificativo sorteggio: ")
	fmt.Println(tag)
	color.Style{color.FgCyan, color.OpBold}.Print("Periodo di riferimento da: ")
	fmt.Println(dataInizio)
	color.Style{color.FgCyan, color.OpBold}.Print("a: ")
	fmt.Println(dataFine)
	color.Style{color.FgCyan, color.OpBold}.Print("Totale possibili estratti: ")
	fmt.Println(listaSceltelen)
	color.Style{color.FgCyan, color.OpBold}.Print("Lista possibili estratti: ")
	printarray(listaScelte)
	color.Style{color.FgWhite, color.OpBold}.Println("-------------------------------------------")
	color.Style{color.FgCyan, color.OpBold}.Print("Data e ora sorteggio: ")
	fmt.Println(dataOra.Format("2006-01-02 15:04:05"))
	color.Style{color.FgCyan, color.OpBold}.Print("Percentuale estratti: ")
	fmt.Printf("%d", percentEstratti)
	fmt.Print("% \n")
	color.Style{color.FgCyan, color.OpBold}.Print("Totale estratti: ")
	fmt.Println(nEstratti)

	for _, v := range randomize[:nEstratti] {
		listaEstratti = append(listaEstratti, listaScelte[v])
	}

	color.Style{color.FgCyan, color.OpBold}.Print("Elenco estratti: ")
	printarray(listaEstratti)
	color.Style{color.FgWhite, color.OpBold}.Println("-------------------------------------------")

	//Scrittura dati nella variabile di tipo ReportOutput struct
	td := ReportOutput{verProgramma, tag, dataInizio, dataFine, listaSceltelen, strings.Join(listaScelte, ", "), dataOra.Format("2006-01-02 15:04:05"), percentEstratti, nEstratti, strings.Join(listaEstratti, ", ")}
	strings.Join(listaScelte, ", ")

	paths := []string{
		"./templates/templatehtml.html",
	}

	templatehtml, err := template.ParseFiles(paths...)
	if err != nil {
		panic(err)
	}

	//err = templatehtml.Execute(os.Stdout, td)
	buf := new(bytes.Buffer)
	err = templatehtml.Execute(buf, td)
	if err != nil {
		panic(err)
	}

	reportsHTMLPath := filepath.Join(".", "reports_html")
	// os.Stat() gets information from the path of the file we want to create and check for if it exists
	// by analyzing if it returned an error with os.IsNotExist(). If it doesn't exist it creates a new folder
	// os.Stat() controlla che il percorso/cartella del file di report che vogliamo creare esista. Se non esiste
	// il risultato viene controllato da os.IsNotExist e la cartella viene creata
	if _, err := os.Stat(reportsHTMLPath); os.IsNotExist(err) {
		errDir := os.MkdirAll(reportsHTMLPath, os.ModePerm)
		if errDir != nil {
			log.Fatal(err)
		}

	}

	timestamp := dataOra.Format("2006-01-02__15-04-05")
	reportHTMLName := timestamp + "__" + tag + "-report.html"
	fileHTMLPath, _ := filepath.Abs(reportsHTMLPath + "/" + reportHTMLName)

	filehtml, err := os.Create(fileHTMLPath)
	if err != nil {
		return
	}
	defer filehtml.Close()

	filehtml.Write(buf.Bytes())

	/* Abilitare i log in scrittura sul file in caso di problemi
	fileHtmlWritten, err := filehtml.Write(buf.Bytes())
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Bytes written: %d\n", fileHtmlWritten)
	*/

	//Generazione report PDF dalla variabile buf usata per generare il file report.html
	pdfg, err := wkhtml.NewPDFGenerator()
	if err != nil {
		return
	}

	pdfg.AddPage(wkhtml.NewPageReader(strings.NewReader(buf.String())))
	// Crea il documento PDF nel buffer interno
	err = pdfg.Create()
	if err != nil {
		log.Fatal(err)
	}

	//Creazione cartella reports_pdf se non esiste
	reportsPDFPath := filepath.Join(".", "reports_pdf")
	if _, err := os.Stat(reportsPDFPath); os.IsNotExist(err) {
		errDir := os.MkdirAll(reportsPDFPath, os.ModePerm)
		if errDir != nil {
			log.Fatal(err)
		}

	}
	//reportPDFName := "report.pdf"
	reportPDFName := timestamp + "__" + tag + "-report.pdf"
	filePDFPath, _ := filepath.Abs(reportsPDFPath + "/" + reportPDFName)

	//Definisce il file PDF in output
	err = pdfg.WriteFile(filePDFPath)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Premere 'Invio' per continuare...")
	bufio.NewReader(os.Stdin).ReadBytes('\n')

}

/*
//reading an integer
var age int
fmt.Println("What is your age?")
_, err: fmt.Scan(&age)

//reading a string
reader := bufio.newReader(os.Stdin)
var name string
fmt.Println("What is your name?")
name, _ := reader.readString("\n")

*/
