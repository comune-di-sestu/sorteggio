# Comune di Sestu - Programma Sorteggio


Software per estrazioni e sorteggi casuali utilizzato dal Comune di Sestu nell'ambito dei sistemi di controlli a campione sulle dichiarazioni rese dagli operatori economici aggiudicatari delle forniture di beni e servizi in merito al possesso dei requisiti di cui all'articolo 80 del decreto legislativo n.50/2016.

Caratteristiche:
- è scritto nel linguaggio Go (https://golang.org/); 
- l'algoritmo di estrazione dei numeri casuali utilizza la libreria "math/rand" (https://golang.org/pkg/math/rand/) della Standard Library del linguaggio di programmazione Go ed utilizza la funzione rand() come generatore di numeri casuali per l'estrazione dei risultati da una lista di elementi rand.Seed(time.Now().UnixNano()); per ottenere un risultato il più casuale possibile, la funzione rand() viene integrata con la funzione "time", in modo che fornisca ad ogni estrazione un numero chiamato seme (seed) sempre nuovo generato a partire dall'ora corrente al momento dell'estrazione;
- prevede che prima di ogni estrazione l'operatore inserisca manualmente in sequenza in apposito database il nome degli operatori da sottoporre ad estrazione;
- prevede che ogni estrazione: 
  - sia individuata da un numero progressivo gestito dall'operatore e dalla data ed orario di estrazione acquisiti automaticamente dal sistema;
  - venga attivata dall'operatore il quale indica il periodo di riferimento;
  - avvenga sulla percentuale indicata dall'operatore; generi un apposito report in formato PDF.


## Prerequisiti

In ambiente Windows:

1) Installare wkhtmltox-0.12.5-1.msvc2015-win64.exe o versione successiva
2) Digitare sysdm.cpl nella barra delle ricerche
3) Cliccare sulla scheda “Avanzate” e poi sul pulsante “Variabili d’ambiente”
4) Nella sezione “Variabili di sistema” selezionare la variabile “Path” e cliccare sul pulsante “Modifica”.
5) Cliccare sul pulsante “Nuovo” e aggiungere la voce:  C:\Program Files\wkhtmltopdf\bin e cliccare “OK”
6) Entrare nella cartella sorteggio_0.2
7) Inserire la lista delle aziende nella prima colonna del file aziende.csv (salvare sempre il file in formato CSV)
8) Fare doppio clic sul file sorteggio.exe  (o main.exe a seconda dell’installazione)
9) Il file generato in HTML si troverà nella cartella reports_html
10) Il file generato in PDF si troverà nella cartella reports_pdf

## Compilazione ed installazione

La compilazione è possibile sia per ambienti Linux/Mac e Windows.

Ambiente Windows

Installare l'ambiente di sviluppo GO  (https://golang.org/doc/install).

Clonare il repository, posizionarsi nella cartella del progetto e digitare: **go build**. Verrà generato un file **main.exe**

Per lanciare il programma fare doppio clic su **main.exe**

## Struttura e file del programma

main.exe       -> eseguibile (sistemi Windows)
main           -> eseguibile (sistemi Linux)
aziende.csv    -> file in CSV dove vanno inserite le aziende o i nominativi da sorteggiare
templates      -> cartella in cui è presente il template html
reports_html   -> cartella in cui vengono generati i report delle estrazioni in formato HTML
reports_pdf    -> cartella in cui vengono generati i report delle estrazioni in formato PDF



#### Logo nel report HTML

Se non si dovesse visualizzare il logo del Comune nel report HTML, aprire il file templatehtml.html nella cartella templates e inserire il percorso assoluto del logo in formato jpg o png nella riga 53 del file.


## Da aggiungere/implementare

- test;
- validazione input nei campi data;





LICENZA: AGPL 3.0

